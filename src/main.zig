const std = @import("std");

fn swap(a: anytype, b: anytype) void {
    const tmp = a.*;
    a.* = b.*;
    b.* = tmp;
}

fn Node(comptime T: type) type {
    return struct {
        const Self = @This();
        const Element = T;

        left: ?*Self,
        right: ?*Self,
        val: T,

        depth: usize = 0,

        fn init(val: Element) Self {
            return Self{ .left = null, .right = null, .val = val };
        }

        fn invertR(self: *Self) void {
            swap(&self.left, &self.right);
            if (self.left) |left| {
                left.invertR();
            }
            if (self.right) |right| {
                right.invertR();
            }
        }

        pub fn printIndent(self: Self, writer: anytype) !void {
            var d = self.depth;
            while (d > 0) {
                try writer.writeAll("  ");
                d -= 1;
            }
        }
        pub fn format(
            self: Self,
            comptime fmt: []const u8,
            options: std.fmt.FormatOptions,
            writer: anytype,
        ) !void {
            _ = fmt;
            _ = options;

            var space = false;
            try self.printIndent(writer);
            try writer.print("{{ {d}", .{self.val});
            if (self.left) |left| {
                try writer.print("\n{}", .{left});
                space = true;
            }
            if (self.right) |right| {
                if (space) {
                    try writer.print("{}", .{right});
                } else {
                    try writer.print("\n{}", .{right});
                }
                space = true;
            }
            if (space) {
                try self.printIndent(writer);
                try writer.writeAll("}\n");
            } else {
                try writer.writeAll(" }\n");
            }
        }
    };
}

fn Tree(comptime T: type) type {
    return struct {
        const Self = @This();
        const N = 64;

        nodes: [N]T = undefined,
        root: ?*T = null,
        size: usize = 0,

        fn newChild(self: *Self, val: T.Element) *T {
            const child = &self.nodes[self.size];
            child.* = T.init(val);
            self.size += 1;
            return child;
        }

        fn insertAfter(self: *Self, parent: *T, val: T.Element) void {
            const p_branch = if (val >= parent.val) &parent.right else &parent.left;
            if (p_branch.*) |branch| {
                self.insertAfter(branch, val);
            } else {
                const child = self.newChild(val);
                child.depth = parent.depth + 1;
                p_branch.* = child;
            }
        }

        fn init() Self {
            return Self{};
        }

        fn insert(self: *Self, val: T.Element) void {
            if (self.root) |root| {
                self.insertAfter(root, val);
            } else {
                self.root = self.newChild(val);
            }
        }

        fn invertRecursive(self: *Self) void {
            if (self.root) |root| {
                root.invertR();
            }
        }

        pub fn format(
            self: Self,
            comptime fmt: []const u8,
            options: std.fmt.FormatOptions,
            writer: anytype,
        ) !void {
            _ = fmt;
            _ = options;

            if (self.root) |root| {
                try writer.print("Tree[{}] \n{}", .{ self.size, root });
            } else {
                try writer.writeAll("Tree[0] {}");
            }
        }
    };
}

pub fn main() anyerror!void {
    var tree = Tree(Node(f64)).init();
    tree.insert(3.14);
    tree.insert(1.23);
    tree.insert(2.54);
    tree.insert(5.55);
    tree.insert(-0.123);
    tree.insert(3.15);
    {
        var i: f64 = 0.0;
        while (i < 10) {
            tree.insert(10.0 + i);
            i += 0.892345;
        }
    }

    std.log.info("Hi! {}", .{tree});
    tree.invertRecursive();
    std.log.info("Inverted! {}", .{tree});

    std.debug.print("As array: [", .{});
    for (tree.nodes[0..tree.size]) |node, i| {
        if (i < tree.size - 1) {
            std.debug.print("{}, ", .{node.val});
        } else {
            std.debug.print("{}]\n", .{node.val});
        }
    }
}
